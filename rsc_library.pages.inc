<?php


function rsc_library_csv_header() {
  return [
    'nid',
    'title',
    'rscl_attachment',
    'rscl_pages',
    'rscl_weight',
    'rscl_editor',
    'rscl_code',
    'rscl_link',
    'rscl_date',
    'rscl_category_library_categories',
    'rscl_author_authors',
    'rscl_source_sources',
    'rscl_tag_tags',
    'rscl_difficulty_difficulty',
    'field_bible_study_category',
    'field_form',
    'field_youth',
    'rscl_format',
    'rscl_featured_image',
    'type',
    'language',
    'url',
    'edit_url',
    'status',
    'promote',
    'sticky',
    'created',
    'changed',
    'author',
    'source',
    'log',
    'revision',
    'comment',
    'comments',
    'comment_count',
    'comment_count_new',
    'body',
    'views',
  ];
}


function rsc_library_csv() {

  // Get all IDs of nodes of rsc_library content types.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', rsc_library_content_types(), 'IN')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (!isset($result['node'])) {
    echo 'No results.';
    return NULL;
  }
  $nids = array_keys($result['node']);

  // Start a batch.
  $batch = [
    'title' => t('Exporting'),
    'operations' => [],
    'finished' => 'rsc_library_csv_finished',
    'file' => drupal_get_path('module', 'rsc_library') . '/rsc_library.pages.inc',
  ];
  $batch['operations'][] = ['rsc_library_csv_start', []];
  foreach ($nids as $nid) {
    $batch['operations'][] = ['rsc_library_csv_export_node', [$nid]];
  }
  batch_set($batch);
  batch_process('/admin/content');
}


function _rsc_library_csv_normalize($data) {
  if ($data instanceof EntityListWrapper) {
    $normalized = [];
    foreach($data as $key => $value) {
      $normalized[$key] = _rsc_library_csv_normalize($value);
    }
    return implode("\n", array_filter($normalized));
  }

  $value = $data->value();
  switch ($data->type()) {
    case 'field_item_file':
    case 'field_item_image':
      return empty($value['filename']) ? NULL : $value['filename'];
    case 'integer':
    case 'text':
    case 'boolean':
    case 'token':
    case 'uri':
    case 'date':
      return trim($value);
    case 'taxonomy_term':
    case 'user':
      return empty($value->name) ? NULL : $value->name;
    case 'node':
      return empty($value->nid) ? NULL : $value->nid;
    case 'text_formatted':
      return empty($value['value']) ? NULL : $value['value'];
  }

  // Ignore others.
  return NULL;
}


function rsc_library_csv_start(&$context) {
  // Start a CSV file
  $context['results']['file_uri'] = "temporary://rsc_library.export.csv";
  $file_pointer = fopen($context['results']['file_uri'], 'w');
  fputcsv($file_pointer, rsc_library_csv_header());
  fclose($file_pointer);
}


function rsc_library_csv_export_node($nid, &$context) {
  // Load and normalize the node
  /**
   * @var \EntityDrupalWrapper
   */
  $node = entity_metadata_wrapper('node', node_load($nid));
  $record = [
    'nid' => $node->nid,
  ];
  foreach ($node->getPropertyInfo() as $property_key => $property_info) {
    $record[$property_key] = _rsc_library_csv_normalize($node->get($property_key));
    if (is_string($record[$property_key])) {
      $record[$property_key] = trim($record[$property_key]);
    }
  }
  // Make sure that every record has the keys in the same order.
  $record = _rsc_library_csv_sort_by_header($record);

  // Append to file
  $file_pointer = fopen($context['results']['file_uri'], 'a');
  fputcsv($file_pointer, $record);
  fclose($file_pointer);
}


function rsc_library_csv_finished($success, $results, $operations) {
  // Serve a CSV file with the current time in the filename.
  $time = new DateTime();
  file_transfer($results['file_uri'], [
    'Content-Type'              => 'text/csv; charset=utf-8',
    'Content-Disposition'       => "attachment; filename=rsc_library_" . $time->format(DateTime::ATOM) . ".csv",
    'Content-Length'            => filesize($results['file_uri']),
    'Content-Transfer-Encoding' => 'binary',
    'Pragma'                    => 'no-cache',
    'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
    'Expires'                   => '0',
    'Accept-Ranges'             => 'bytes'
  ]);
}


function _rsc_library_csv_sort_by_header($record) {
  $sorted_record = [];
  foreach (rsc_library_csv_header() as $key) {
    $sorted_record[$key] = isset($record[$key]) ? $record[$key] : NULL;
  }
  return $sorted_record;
}
